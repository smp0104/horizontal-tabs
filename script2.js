window.Dell = window.Dell || {};
Dell.CrossSell = {EnableCategoryTabsV2:true};
Dell.CrossSell.CategoryTabs = Dell.CrossSell.CategoryTabs || {};

(function ($, document) {

    Dell.CrossSell.CategoryTabs = (function () {
        'use strict';
        let leftButton = "#acs-tab-prev",
            rightButton = "#acs-tab-next",
            navList = "#acs-category-tab-list";
        let categoryTabs = {
            init: function () {
                const categoryId = $("#acs-category-tab-list").find('li:nth-child(1) button').data("categoryid");
                const categoryTabsElement = document.getElementById("acs-category-tabs");
                categoryTabsElement.addEventListener("ddsTabsOpenedEvent", function (e) {
                    e.preventDefault();
                    Dell.CrossSell.CategoryTabs.openedTabCallback(e);
                });
                if (Dell.CrossSell.EnableCategoryTabsV2 == true) {
                    $(document).on("click", leftButton, function () { slideNavBar("left"); });
                    $(document).on("click", rightButton, function () { slideNavBar("right"); });                    
                    $(navList).on("scroll", function () { enableDisableArrowButtons(); });                    

                    showHideArrowButtons();
                    enableDisableArrowButtons();
                    window.addEventListener("resize", function () {
                        showHideArrowButtons(); 
                        enableDisableArrowButtons();
                    });
                }
            },
            openedTabCallback: function (event) {
                $(".dds__tabs__pane-container .acs_pane-none").removeClass("acs_pane-none");
                if (event && event.detail && event.detail.index >= 0) {
                    let topOffset = $("#acs-tabs-pane-section").offset().top;
                    if ($('.acs__tabs_hz_menu_fixed').length > 0) {
                        $('html, body').stop()
                            .animate({ scrollTop: topOffset }, 50);
                    }
                }
            },
            loadImages: function (categoryId) {
                let imgs = $(`#acs-category-stack-content-${categoryId} img[data-src]`);
                if (Dell.lazyImg && imgs.length > 0) {
                    Dell.lazyImg.load(imgs);
                }
            }
        };

        function slideNavBar(direction) {            
            let left = 0;
            let navtabs = document.querySelector(navList);
            let slideTimer = setInterval(function () {
                direction === "left" ? navtabs.scrollLeft -= 10 : navtabs.scrollLeft += 10;
                left += 10;
                if (left >= 100) {
                    window.clearInterval(slideTimer);
                }
            }, 25);
        }

        function showHideArrowButtons() {
            if ($(".dds__tabs__list > li").length <= 1) {
                return;
            }
            let navtabs = document.querySelector(navList);
            let isEqual = (navtabs.offsetWidth) === navtabs.scrollWidth;            
            if (isEqual) {
                $(leftButton).addClass("dds__d-none");
                $(rightButton).addClass("dds__d-none");
            }
            else {
                $(leftButton).removeClass("dds__d-none");
                $(rightButton).removeClass("dds__d-none");
            }                      
        }
        function enableDisableArrowButtons() {            
            let navtabs = document.querySelector(navList);
            let scrollLeft = navtabs.scrollLeft;
            let offsetWidth = navtabs.offsetWidth;
            let scrollWidth = navtabs.scrollWidth;               
            if (scrollLeft === 0) {                
                $(leftButton).addClass("acs__tab-arrow-disabled");
            } else {                
                $(leftButton).removeClass("acs__tab-arrow-disabled");
            }
            if (scrollWidth - scrollLeft - offsetWidth <= 1) {                
                $(rightButton).addClass("acs__tab-arrow-disabled");
            } else {                
                $(rightButton).removeClass("acs__tab-arrow-disabled");
            }            
        }
        return categoryTabs;
    })();

    $(document).ready(function () {
        Dell.CrossSell.CategoryTabs.init();
        if (Dell.CrossSell.EnableCategoryTabsV2 == true) {
            var lastScroll = 0;
            var posFixed = 0;
            var lastPostFixedScroll = 0;
            var dbTimer;
            $(window).scroll(function (event) {
                event.preventDefault();
                if (window.innerWidth > 768) {
                    return;
                }
                if (dbTimer) {
                    window.clearTimeout(dbTimer);
                }
                dbTimer = window.setTimeout(function () {
                    var elementPosition = $('.dds__tabs__list-container').offset();
                    var nowScrollTop = $(window).scrollTop();
        
                    if (posFixed == 1 && lastPostFixedScroll > lastScroll) {
                        elementPosition.top = lastPostFixedScroll;
                        posFixed = 0;
                    }
                    if (nowScrollTop >= elementPosition.top) {
                        $('.dds__tabs__list-container').addClass('acs__tabs_hz_menu_fixed');
                        $('.acs-subtotal-fixed').addClass('acs__tab-hz-subtotal');
                    }
                    else {
                        $('.dds__tabs__list-container').removeClass('acs__tabs_hz_menu_fixed');
                        $('.acs-subtotal-fixed').removeClass('acs__tab-hz-subtotal');
                    }
                    lastScroll = nowScrollTop;
                    if (posFixed == 0 && nowScrollTop == elementPosition.top) {
                        posFixed = 1;
                        lastPostFixedScroll = lastScroll;
                    }
                }, 0);
            });
        }        
    });
})(jQuery, document);