window.Dell = window.Dell || {};
Dell.CrossSell = {EnableCategoryTabsV2:true};
Dell.CrossSell.CategoryTabs = Dell.CrossSell.CategoryTabs || {};

(function ($, document) {

    Dell.CrossSell.CategoryTabs = (function () {
        'use strict';
        let leftButton = "#acs-tab-prev",
            rightButton = "#acs-tab-next",
            navList = "#acs-category-tab-list";
        let categoryTabs = {
            init: function () {
                const categoryId = $("#acs-category-tab-list").find('li:nth-child(1) button').data("categoryid");
                const categoryTabsElement = document.getElementById("acs-category-tabs");
                categoryTabsElement.addEventListener("ddsTabsOpenedEvent", function (e) {
                    e.preventDefault();
                    Dell.CrossSell.CategoryTabs.openedTabCallback(e);
                });
                if (Dell.CrossSell.EnableCategoryTabsV2 == true) {
                    $(document).on("click", leftButton, function () { slideNavBar("left"); });
                    $(document).on("click", rightButton, function () { slideNavBar("right"); });                    
                    $(navList).on("scroll", function () { enableDisableArrowButtons(); });                    

                    showHideArrowButtons();
                    enableDisableArrowButtons();
                    window.addEventListener("resize", function () {
                        showHideArrowButtons(); 
                        enableDisableArrowButtons();
                    });
                }
            },
            openedTabCallback: function (event) {
                $(".dds__tabs__pane-container .acs_pane-none").removeClass("acs_pane-none");
                if (event && event.detail && event.detail.index >= 0) {
                    let topOffset = $("#acs-tabs-pane-section").offset().top;
                    if ($('.acs__tabs_hz_menu_fixed').length > 0) {
                        $('html, body').stop()
                            .animate({ scrollTop: topOffset }, 50);
                    }
                }
            },
            loadImages: function (categoryId) {
                let imgs = $(`#acs-category-stack-content-${categoryId} img[data-src]`);
                if (Dell.lazyImg && imgs.length > 0) {
                    Dell.lazyImg.load(imgs);
                }
            }
        };

        function slideNavBar(direction) {            
            let left = 0;
            let navtabs = document.querySelector(navList);
            let slideTimer = setInterval(function () {
                direction === "left" ? navtabs.scrollLeft -= 10 : navtabs.scrollLeft += 10;
                left += 10;
                if (left >= 100) {
                    window.clearInterval(slideTimer);
                }
            }, 25);
        }

        function showHideArrowButtons() {
            if ($(".dds__tabs__list > li").length <= 1) {
                return;
            }
            let navtabs = document.querySelector(navList);
            let isEqual = (navtabs.offsetWidth) === navtabs.scrollWidth;            
            if (isEqual) {
                $(leftButton).addClass("dds__d-none");
                $(rightButton).addClass("dds__d-none");
            }
            else {
                $(leftButton).removeClass("dds__d-none");
                $(rightButton).removeClass("dds__d-none");
            }                      
        }
        function enableDisableArrowButtons() {            
            let navtabs = document.querySelector(navList);
            let scrollLeft = navtabs.scrollLeft;
            let offsetWidth = navtabs.offsetWidth;
            let scrollWidth = navtabs.scrollWidth;               
            if (scrollLeft === 0) {                
                $(leftButton).addClass("acs__tab-arrow-disabled");
            } else {                
                $(leftButton).removeClass("acs__tab-arrow-disabled");
            }
            if (scrollWidth - scrollLeft - offsetWidth <= 1) {                
                $(rightButton).addClass("acs__tab-arrow-disabled");
            } else {                
                $(rightButton).removeClass("acs__tab-arrow-disabled");
            }            
        }
        return categoryTabs;
    })();

    $(document).ready(function () {
        Dell.CrossSell.CategoryTabs.init();
        if (Dell.CrossSell.EnableCategoryTabsV2 == true) {
            var lastScroll = 0;
            var posFixed = 0;
            var lastPostFixedScroll = 0;
            $(window).scroll(function () {
                if (window.innerWidth > 768) {
                    return;
                }

                // var elementPosition = $('.dds__tabs__list-container').offset();
                // var nowScrollTop = $(window).scrollTop();

                var pageYOffset = window.pageYOffset;
                var elementTopPosition = document.querySelector('.dds__tabs__list-container').offsetTop;

                console.log("1-js window.pageYOffset = "+pageYOffset);
                console.log("1.1-jq nowScrollTop = "+ $(window).scrollTop());
               
                console.log("2-js elementTopPosition = "+elementTopPosition);
                console.log("2.1-jq elementPosition top = "+$('.dds__tabs__list-container').offset().top);         
                
                
                if (pageYOffset+1 >= elementTopPosition) {
                    console.log("fixed");
                    $('.dds__tabs__list-container').addClass('acs__tabs_hz_menu_fixed');
                    $('.acs-subtotal-fixed').addClass('acs__tab-hz-subtotal');
                }
                else {
                    console.log("removed fixed");
                    $('.dds__tabs__list-container').removeClass('acs__tabs_hz_menu_fixed');
                    $('.acs-subtotal-fixed').removeClass('acs__tab-hz-subtotal');
                }


                // console.log("1-nowScrollTop = "+nowScrollTop);
                // console.log("2-Before set elementPosition.top = "+elementPosition.top);
                // if (posFixed == 1 && lastPostFixedScroll > lastScroll) {
                //     elementPosition.top = lastPostFixedScroll;
                //     posFixed = 0;
                // }
                // console.log("3-Before if-else elementPosition.top = "+elementPosition.top);
                // if (nowScrollTop >= elementPosition.top) {
                //     console.log("fixed");
                //     $('.dds__tabs__list-container').addClass('acs__tabs_hz_menu_fixed');
                //     $('.acs-subtotal-fixed').addClass('acs__tab-hz-subtotal');
                // }
                // else {
                //     console.log("removed fixed");
                //     $('.dds__tabs__list-container').removeClass('acs__tabs_hz_menu_fixed');
                //     $('.acs-subtotal-fixed').removeClass('acs__tab-hz-subtotal');
                // }
                // lastScroll = nowScrollTop;
                // if (posFixed == 0 && nowScrollTop == elementPosition.top) {
                //     posFixed = 1;
                //     lastPostFixedScroll = lastScroll;
                // }
               
            });
        }

       

        
        
    });
})(jQuery, document);